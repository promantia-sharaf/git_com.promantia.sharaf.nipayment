/*
 * ***********************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U. Licensed under the Openbravo Commercial License version
 * 1.0 You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html or in the
 * legal folder of this module distribution.
 * ***********************************************************************************
 * 
 * 
 * package com.promantia.sharaf.nipayment;
 * 
 * import javax.enterprise.context.ApplicationScoped;
 * 
 * import org.codehaus.jettison.json.JSONObject; import org.openbravo.model.common.order.Order;
 * import org.openbravo.model.financialmgmt.payment.FIN_Payment; import
 * org.openbravo.retail.posterminal.OrderLoaderPreProcessPaymentHook;
 * 
 * @ApplicationScoped public class NIOrderLoaderPostProcessPayment extends
 * OrderLoaderPreProcessPaymentHook { public void exec(JSONObject jsonorder, Order order, JSONObject
 * jsonpayment, FIN_Payment payment) throws Exception {
 * 
 * if (jsonpayment.has("custsniCardDetails")) { String cardDetails =
 * jsonpayment.getString("custsniCardDetails"); payment.setSharccCardDetails(cardDetails); } if
 * (jsonpayment.has("custsniCardApprovalCode")) { String sharccCardAuthcode =
 * jsonpayment.getString("custsniCardApprovalCode");
 * payment.setSharccCardAuthcode(sharccCardAuthcode); } } }
 */