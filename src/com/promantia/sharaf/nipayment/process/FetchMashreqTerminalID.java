package com.promantia.sharaf.nipayment.process;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.retail.posterminal.OBPOSApplications;

public class FetchMashreqTerminalID extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(FetchMashreqTerminalID.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    // TODO Auto-generated method stub

    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();

    try {
      OBPOSApplications posTerminal = OBDal.getInstance().get(OBPOSApplications.class,
          jsonsent.get("posTerminalID"));

      data.put("terminalID", posTerminal.getCustsniTerminalid());
      result.put("status", 0);
      result.put("data", data);
    } catch (Exception e) {
      log.error("Error getting Mashreq terminalIDfrom Payment Methods: " + e.getMessage(), e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    }

    return result;
  }

}
