/*global enyo, $ 

enyo.kind({
  name: 'CUSTSNI_payment.NIPaymentProvider',
  events: {
	    onHideThisPopup: ''
	  },
  components: [{
	    components: [{
	      classes: 'row-fluid',
	      components: [{
	        classes: 'span6',
	        content: 'Payment type'
	      }, {
	        name: 'paymenttype',
	        classes: 'span6',
	        style: 'font-weight: bold;'
	      }]
	    }, {      
	      classes: 'row-fluid',
	      components: [{
	        classes: 'span6',
	        style: 'float: left;',
	        content: 'Amount'
	      }, {
	        name: 'paymentamount',
	        classes: 'span6',
	        style: 'font-weight: bold;'
	      }]
	    }]
	  }, {
	    kind: 'CUSTSNI_payment.NIPaymentProvider_OkButton',
	    ontap: 'confirmPayment'
	}],
  
  initComponents: function () {
    this.inherited(arguments);
    this.owner.owner.closeOnEscKey = false;
    this.owner.owner.autoDismiss = false;
    this.owner.owner.$.headerCloseButton.show();
    this.$.paymenttype.setContent(this.paymentType);
    this.$.paymentamount.setContent(this.paymentAmount);
  },

  confirmPayment: function () {
    var me = this;

    var paymentinfo = {
    };
    //var type = 0;        
    var request = {
    };
    request = paymentinfo.refund ? this.populateRefundRequest(request, paymentinfo) : this.populatePaymentRequest(request, paymentinfo);
    return this.remoteRequest(request, me);

  },

  populatePaymentRequest: function (request, exceptioninfo) {
    return request;
  },
  populateRefundRequest: function (request, exceptioninfo) {
    return request;
  },

  remoteRequest: function (request, me) {
  }
});

enyo.kind({
	  kind: 'OB.UI.ModalDialogButton',
	  name: 'CUSTSNI_payment.NIPaymentProvider_OkButton',
	  style: 'float: right;',
	  i18nContent: 'OBMOBC_LblOk',
	  isDefaultAction: true
	});
*/