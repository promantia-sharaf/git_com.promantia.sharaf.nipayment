
//****************** extending OB.UI.ModalProviderGroup for adding NI payments *****************************//
/*global $, _ */

/*OB.UI.ModalProviderGroup.extend({
	startPaymentRefund : function(){

	    var receipt = this.args.receipt;
	    var amount = this.args.amount;
	    var refund = this.args.refund;
	    var currency = this.args.currency;
	    var providerGroup = this.args.providerGroup;
	    var providerinstance = this.args.providerinstance;
	    var attributes = this.args.attributes;
	    var i;

	    this.$.bodyContent.$.providergroupcomponent.destroyComponents();
	    if (providerinstance.providerComponent) {
	      this.$.bodyContent.$.providergroupcomponent
	        .createComponent(providerinstance.providerComponent)
	        .render();
	    }

	    if (providerinstance.checkOverpayment && !refund) {
	      // check over payments in all payments of the group
	      for (i = 0; i < providerGroup._payments.length; i++) {
	        var payment = providerGroup._payments[i];

	        if (!payment.paymentMethod.allowoverpayment) {
	          this.showMessageAndClose(
	            OB.I18N.getLabel('OBPOS_OverpaymentNotAvailable')
	          );
	          return;
	        }

	        if (_.isNumber(payment.paymentMethod.overpaymentLimit) &&
	          amount > (receipt.get('gross') + payment.paymentMethod.overpaymentLimit - receipt.get('payment')).toFixed(OB.MobileApp.model.get('currency').pricePrecision)) {
	          this.showMessageAndClose(OB.I18N.getLabel('OBPOS_OverpaymentExcededLimit'));
	          return;
	        }
	      }
	    }

	    providerinstance
	      .processPayment({
	        receipt: receipt,
	        currency: currency,
	        amount: OB.DEC.mul(amount,100), //make the amount in device same as displaying in POS for NI device
	        refund: refund,
	        providerGroup: providerGroup
	      })
	      .then(
	    		  response => {
	    			console.log('response from device for adding NI payment is:');
	  	        	console.log(response);
	    			  if(response.resultCode !== '00' && response.resultCode !== '000'){ //to add payment to ticket only if device response is success
	    				  this.showMessageAndClose(OB.I18N.getLabel('OBPOS_TransactionError'));
	    			  }else {

	  	          //setting void properties
	  	          response.properties.voidproperties = {};
	  	          response.properties.voidproperties.ecrReceiptNumber = response.properties.ecrReceiptNumber;
	  	          response.properties.voidproperties.sequenceNumber = response.properties.sequenceNumber;
	  	          response.properties.voidproperties.approvalCode = response.properties.approvalCode;
	  	          response.properties.voidproperties.cardNumber = response.properties.cardNumber;
	    			  const addResponseToPayment = payment => {
	    				          // We found the payment method that applies.
	    				          const paymentline = {
	    				            kind: payment.payment.searchKey,
	    				            name: payment.payment._identifier,
	    				            amount: amount,
	    				            rate: payment.rate,
	    				            mulrate: payment.mulrate,
	    				            isocode: payment.isocode,
	    				            allowOpenDrawer: payment.paymentMethod.allowopendrawer,
	    				            isCash: payment.paymentMethod.iscash,
	    				            openDrawer: payment.paymentMethod.openDrawer,
	    				            printtwice: payment.paymentMethod.printtwice,
	    				            paymentData: {
	    				              provider: providerGroup.provider,
	    				              voidConfirmation: false,
	    				              // Is the void provider in charge of defining confirmation.
	    				              transaction: response.transaction,
	    				              authorization: response.authorization,
	    				              properties: response.properties
	    				            },
	    				            custsniCardDetails: response.properties.cardNumber,
	    				            custsniCardApprovalCode: response.properties.approvalCode
	    				          };
	    				          receipt.addPayment(
	    				          new OB.Model.PaymentLine(Object.assign(paymentline, attributes)));
	    				          window.setTimeout(this.doHideThisPopup.bind(this), 0);
	    				        };

	    				        // First attempt. Find an exact match.
	    				        const cardlogo = response.properties.cardlogo;
	    				        let undefinedPayment = null;
	  	          for (i = 0; i < providerGroup._payments.length; i++) {
	  	        	const payment = providerGroup._payments[i];
	  	            if (cardlogo === payment.paymentType.searchKey) {
	  	            	addResponseToPayment(payment);
	  	            	return; // Success
	  	            } else if ('UNDEFINED' === payment.paymentType.searchKey) {
	  	                 undefinedPayment = payment;
	  	            }
	  	          }

	  	     // Second attempt. Find UNDEFINED paymenttype.
	  	              if (undefinedPayment) {
	  	                addResponseToPayment(undefinedPayment);
	  	                return; // Success
	  	              }

	  	              // Fail. Cannot find payment to assign response
	  	              this.showMessageAndClose(
	  	              OB.I18N.getLabel('OBPOS_CannotFindPaymentMethod'));
	    			 }
	  	            }).
	  	            catch (exception => {
	  	              this.showMessageAndClose(
	  	              providerinstance.getErrorMessage ? providerinstance.getErrorMessage(exception) : exception.message);
	  	            });
	}
});*/

//****************** extending OB.UI.ModalProviderGroupVoid for voiding NI payments *******************
/*global $, _ */

/*OB.UI.ModalProviderGroupVoid.extend({
	startVoid: function() {
	    var payment = this.args.payment;
	    var amount = payment.get('amount');
	    var provider = payment.get('paymentData').provider;
	    var providerinstance = this.args.providerinstance;

	    var receipt = this.args.receipt;
	    var removeTransaction = this.args.removeTransaction;

	    this.$.bodyContent.$.providergroupcomponent.destroyComponents();
	    if (providerinstance.providerComponent) {
	      this.$.bodyContent.$.providergroupcomponent
	        .createComponent(providerinstance.providerComponent)
	        .render();
	    }

	    providerinstance
	      .processVoid({
	        receipt: receipt,
	        payment: payment
	      })
	      .then(
	        function(response) {
	        	console.log('response from device for voiding the NI payment is:');
	        	console.log(response);
	        	if(response.resultCode !== '00' && response.resultCode !== '000'){ //To void the payment only if device response is success 
	        		this.showMessageAndClose(OB.I18N.getLabel('OBPOS_TransactionError'));
	        	}else{
	        		removeTransaction();
	        		window.setTimeout(this.doHideThisPopup.bind(this), 0);
	        	}
	        }.bind(this)
	      )
	      ['catch'](
	        function(exception) {
	          this.showMessageAndClose(
	            providerinstance.getErrorMessage
	              ? providerinstance.getErrorMessage(exception)
	              : exception.message
	          );
	        }.bind(this)
	      );
	  }
});*/


//******************* Extnding OBPOS_StandardProvider for adding terminal ID in the request for Mashreq Integration*********************//

/*OBPOS_StandardProvider.extend({
	
	processPayment: function(paymentinfo){
		var type = paymentinfo.refund
		? OBPOS_StandardProvider.TYPE_REFUND
				: OBPOS_StandardProvider.TYPE_SALE;

		var request = {
				type: type,
				currency: paymentinfo.currency,
				amount: paymentinfo.amount,
				terminal: JSON.parse(localStorage.getItem('mashreqTerminalId')).terminalID,
				properties: {
					provider: paymentinfo.providerGroup.provider.provider
				}
		};

		request = paymentinfo.refund
		? this.populateRefundRequest(request, paymentinfo)
				: this.populatePaymentRequest(request, paymentinfo);

		return OBPOS_StandardProvider.remoteRequest(request);
	}
});
*/



//******************* Extending OBPOS_StandardProviderVoid for adding terminal ID in the void request for Mashreq Integration*********************//

/*OBPOS_StandardProviderVoid.extend({
	
	processVoid: function(voidpaymentinfo) {
	    var request = {
	      type: OBPOS_StandardProvider.TYPE_VOID,
	      currency: voidpaymentinfo.payment.get('isocode'),
	      amount: voidpaymentinfo.payment.get('amount'),
	      terminal: JSON.parse(localStorage.getItem('mashreqTerminalId')).terminalID,
	      properties: voidpaymentinfo.payment.get('paymentData').properties
	        .voidproperties
	    };

	    request = this.populateVoidRequest(request, voidpaymentinfo);

	    return OBPOS_StandardProvider.remoteRequest(request);
	  }
});*/